
/*La estructura de repeticion mas conveniente fue la do while ya que segun la consigna siempre voy a ejecutar una accion que es la de lectura del valor ingresado y ademas no tengo definido  previamente la cantidad de veces en las que se debe repetir dicha accion.*/

#include<stdio.h>


int main(void)

{   int valor1;
    int total;
    char seguir;
    
    valor1 = 0;
    total = 0;
    
    do
    {
    printf("\nIngresar un valor:\n");
    scanf(" %d", &valor1);
    total = total + valor1;
    printf("\nDesea ingresar otro valor s/n?:\n");
    scanf(" %c", &seguir);
    } 
    while(seguir == 's');
        
    printf("\nEl resultado de la suma es: %d \n", total);
    
    return 0;
    
}
    
